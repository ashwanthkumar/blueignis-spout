<?php

/**
 *	Model representing the TwitterUser
 */
class TwitterUser {
	public $id;
	public $twitter_id;
	public $name;
	public $description;
	public $url;
	public $screen_name;
	public $followers_count;
	public $friends_count;
	public $profile_image_url;
	
	private $db;
	public static $_tableName = "twitter_users";
	
	public function TwitterUser($props, $db = NULL) {
		if($this->db == NULL && $db != NULL) $this->db = $db;
		
		$this->twitter_id = $props['id'];
		$this->name = $props['name'];
		$this->description = $props['description'];
		$this->url = $props['url'];
		$this->screen_name = $props['screen_name'];
		$this->followers_count = $props['followers_count'];
		$this->friends_count = $props['friends_count'];
		$this->profile_image_url = $props['profile_image_url'];
	}
	
	public function save($db = NULL) {
		if($this->db == NULL && $db != NULL) $this->db = $db;
		
		$dataToSave = array(
			"twitter_id" => $this->twitter_id,
			"name" => $this->name,
			"description" => $this->description,
			"url" => $this->url,
			"screen_name" => $this->screen_name,
			"followers_count" => $this->followers_count,
			"friends_count" => $this->friends_count,
			"profile_image_url" => $this->profile_image_url
		);
		
		$this->db->insert(TwitterUser::$_tableName, $dataToSave);
		$this->id = $this->db->lastInsertId();
	}
	
	public static function load($props, $db = NULL) {
		$user = $db->select(TwitterUser::$_tableName, "twitter_id = :tuserid", array(":tuserid" => $props['id']));
		if(count($user) < 1) {
			// User not found so create one
			$twitterUser = new TwitterUser($props, $db);
			$twitterUser->save();
			
			return $twitterUser;
		} else {
			$user = $user[0];
			$twitterUser = new TwitterUser($props, $db);
			$twitterUser->id = $user['id'];
			$twitterUser->twitter_id = $user['twitter_id'];
			
			return $twitterUser;
		}
	}
}
