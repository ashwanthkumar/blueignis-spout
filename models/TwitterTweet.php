<?php

/**
 *	Model representating the TwitterTweets
 */
class TwitterTweet {
	public $id;
	public $text;
	public $twitter_status_id;
	public $posted_on;
	public $twitter_user;
	public $campaign_id;
	public $source;
	
	private $db;
	public static $_tableInfo = "tweets";
	
	public function TwitterTweet($props, $twitterUser, $campaignId, $db = NULL) {
		if($this->db == NULL && $db != NULL) $this->db = $db;
		
		$this->text = $props['text'];
		$this->twitter_status_id = $props['id'];
		$this->posted_on = date('Y-m-d H:i:s', strtotime($props['created_at']));
		$this->twitter_user = $twitterUser->id;
		$this->campaign_id = $campaignId;
		$this->source = $props['source'];
	}
	
	public function save($db=NULL) {
		if($this->db == NULL && $db != NULL) $this->db = $db;
		
		$dataToSave = array(
			"text" => $this->text,
			"twitter_status_id" => $this->twitter_status_id,
			"posted_on" => $this->posted_on,
			"twitter_user" => $this->twitter_user,
			"campaign_id" => $this->campaign_id,
			"source" => $this->source
		);
		
		$this->db->insert(TwitterTweet::$_tableInfo, $dataToSave);
		$this->id = $this->db->lastInsertId();
	}
	
	public static function load($props, $twitterUser, $campaign_id, $db = NULL) {
		$tweet = $db->select(TwitterTweet::$_tableInfo, "twitter_status_id = :tstatusid and twitter_users_id = :tuserid and campaign_id = :cid", array(":tstatusid" => $props['id'], ":tuserid" => $twitterUser->id, ":cid" => $campaign_id));
		
		if(count($tweet) < 1) {
			// Tweet does not exist so create one
			$twitterTweet = new TwitterTweet($props, $twitterUser, $campaign_id, $db);
			$twitterTweet->save();
			
			return $twitterTweet;
		} else {
			$tweet = $tweet[0];
			$twitterTweet = new TwitterTweet($tweet, $twitterUser, $campaign_id, $db);
			$twitterTweet->id = $tweet['id'];
			$twitterTweet->twitter_status_id = $tweet['twitter_status_id'];
			$twitterTweet->posted_on = $tweet['posted_on'];
			
			return $twitterTweet;
		}
	}
}
