<?php

/**
 *	Model representating the TwitterFirehouses
 */
class TwitterFirehouse {
	public $id;
	public $text;
	public $twitter_status_id;
	public $posted_on;
	public $twitter_user;
	public $source;
	
	private $db;
	public static $_tableInfo = "twitter_firehouse";
	
	public function TwitterFirehouse($props, $twitterUser, $db = NULL) {
		if($this->db == NULL && $db != NULL) $this->db = $db;
		
		$this->text = $props['text'];
		$this->twitter_status_id = $props['id'];
		$this->posted_on = date('Y-m-d H:i:s', strtotime($props['created_at']));
		$this->twitter_user = $twitterUser->id;
		$this->source = $props['source'];
	}
	
	public function save($db=NULL) {
		if($this->db == NULL && $db != NULL) $this->db = $db;
		
		$dataToSave = array(
			"text" => $this->text,
			"twitter_status_id" => $this->twitter_status_id,
			"posted_on" => $this->posted_on,
			"twitter_users_id" => $this->twitter_user,
			"source" => $this->source,
		);
		
		$this->db->insert(TwitterFirehouse::$_tableInfo, $dataToSave);
		$this->id = $this->db->lastInsertId();
	}
	
	public static function load($props, $twitterUser, $db = NULL) {
		$tweet = $db->select(TwitterFirehouse::$_tableInfo, "twitter_status_id = :tstatusid and twitter_users_id = :tuserid", array(":tstatusid" => $props['id'], ":tuserid" => $twitterUser->id));
		
		if(count($tweet) < 1) {
			// Tweet does not exist so create one
			$TwitterFirehouse = new TwitterFirehouse($props, $twitterUser, $db);
			$TwitterFirehouse->save();
			
			return $TwitterFirehouse;
		} else {
			$tweet = $tweet[0];
			$TwitterFirehouse = new TwitterFirehouse($tweet, $twitterUser, $db);
			$TwitterFirehouse->id = $tweet['id'];
			$TwitterFirehouse->twitter_status_id = $tweet['twitter_status_id'];
			$TwitterFirehouse->posted_on = $tweet['posted_on'];
			
			return $TwitterFirehouse;
		}
	}
}
