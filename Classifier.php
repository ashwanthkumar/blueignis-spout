<?php
require_once("config.php");

/**
 *	Classifier that implements the Naieve Bayesian Algorithm to classify text using the MySQL
 *
 *	@author	Ashwanth Kumar <ashwanth@ashwanthkumar.in>
 **/
class Classifier {
	/**
	 *	PDO Instance to be used within the Classifier instance
	 **/
	private $db;
	private $stop_words;
	
	/**
	 *	Default Constructor
	 */
	public function Classifier($db) {
		// Initialize the PDO Object
		$this->db = $db;
		
		// MySQL Full text search stop word list
		// @TODO: Need to look into it more deeply to find out any sentiment related words can be found.
		$this->stop_words = "a�s, able, about, above, according, accordingly, across, actually, after, afterwards, again, ain�t, all, allow, allows, almost, alone, along, already, also, although, always, am, among, amongst, an, and, another, any, anybody, anyhow, anyone, anything, anyway, anyways, anywhere, apart, appear, appreciate, appropriate, are, aren�t, around, as, aside, ask, asking, associated, at, available, away, awfully, be, became, because, become, becomes, becoming, been, before, beforehand, behind, being, believe, below, beside, besides, best, better, between, beyond, both, brief, but, by, c�mon, c�s, came, can, can�t, cannot, cant, cause, causes, certain, certainly, changes, clearly, co, com, come, comes, concerning, consequently, consider, considering, contain, containing, contains, corresponding, could, couldn�t, course, currently, definitely, described, despite, did, didn�t, different, do, does, doesn�t, doing, don�t, done, down, downwards, during, each, edu, eg, eight, either, else, elsewhere, enough, entirely, especially, et, etc, even, ever, every, everybody, everyone, everything, everywhere, ex, exactly, example, except, far, few, fifth, first, five, followed, following, follows, for, former, formerly, forth, four, from, further, furthermore, get, gets, getting, given, gives, go, goes, going, gone, got, gotten, greetings, had, hadn�t, happens, hardly, has, hasn�t, have, haven�t, having, he, he�s, hello, help, hence, her, here, here�s, hereafter, hereby, herein, hereupon, hers, herself, hi, him, himself, his, hither, hopefully, how, howbeit, however, i�d, i�ll, i�m, i�ve, ie, if, ignored, immediate, in, inasmuch, inc, indeed, indicate, indicated, indicates, inner, insofar, instead, into, inward, is, isn�t, it, it�d, it�ll, it�s, its, itself, just, keep, keeps, kept, know, knows, known, last, lately, later, latter, latterly, least, less, lest, let, let�s, like, liked, likely, little, look, looking, looks, ltd, mainly, many, may, maybe, me, mean, meanwhile, merely, might, more, moreover, most, mostly, much, must, my, myself, name, namely, nd, near, nearly, necessary, need, needs, neither, never, nevertheless, new, next, nine, no, nobody, non, none, noone, nor, normally, not, nothing, novel, now, nowhere, obviously, of, off, often, oh, ok, okay, old, on, once, one, ones, only, onto, or, other, others, otherwise, ought, our, ours, ourselves, out, outside, over, overall, own, particular, particularly, per, perhaps, placed, please, plus, possible, presumably, probably, provides, que, quite, qv, rather, rd, re, really, reasonably, regarding, regardless, regards, relatively, respectively, right, said, same, saw, say, saying, says, second, secondly, see, seeing, seem, seemed, seeming, seems, seen, self, selves, sensible, sent, serious, seriously, seven, several, shall, she, should, shouldn�t, since, six, so, some, somebody, somehow, someone, something, sometime, sometimes, somewhat, somewhere, soon, sorry, specified, specify, specifying, still, sub, such, sup, sure, t�s, take, taken, tell, tends, th, than, thank, thanks, thanx, that, that�s, thats, the, their, theirs, them, themselves, then, thence, there, there�s, thereafter, thereby, therefore, therein, theres, thereupon, these, they, they�d, they�ll, they�re, they�ve, think, third, this, thorough, thoroughly, those, though, three, through, throughout, thru, thus, to, together, too, took, toward, towards, tried, tries, truly, try, trying, twice, two, un, under, unless, unlikely, until, unto, up, upon, us, use, used, useful, uses, using, usually, value, various, very, via, viz, vs, want, wants, was, wasn�t, way, we, we�d, we�ll, we�re, we�ve, welcome, well, went, were, weren�t, what, what�s, whatever, when, whence, whenever, where, where�s, whereafter, whereas, whereby, wherein, whereupon, wherever, whether, which, while, whither, who, who�s, whoever, whole, whom, whose, why, will, willing, wish, with, within, without, won�t, wonder, would, would, wouldn�t, yes, yet, you, you�d, you�ll, you�re, you�ve, your, yours, yourself, yourselves, zero";
		
		$this->stop_words = '/\b(' . implode("|", explode(",",$this->stop_words)) . ')\b/';
	}
	
	/**
	 *	Teach mode of the classifier where the classifier tries to learn from the $corpus for the given $class. 
	 *
	 *	@param	$corpus	String	Document/text to learn from 
	 *	@param	$class	String	Name of the classifier. (It must exist on the `classifier_class` table, else this method will return null)
	 *
	 *	@return	Boolean	TRUE	When the operation has completed successfully.
	 *					FALSE	When there is an error in the operation.
	 */
	public function teach($corpus, $class) {
		// Find if the $class exist
		$classes = $this->db->select("classifier_class", "name = :name", array(":name" => $class));
		if(count($classes) < 1) return FALSE;
		
		// Get the array instance of the classifier 
		$class = $classes[0];
		
		// Clean the corpus
		$filtered_corpus = $this->filter($corpus);
		// Generate uni-grams from the cleaned corpus
		$tokens = $this->tokenise($filtered_corpus);

		foreach($tokens as $token) {
			// Check if the token is already present in the KnowledgeBase
			$tokensFromKB = $this->db->select("classifier_db", "corpus = :token and classifier_class_id = :class_id", array(":token" => $token, ":class_id" => $class['id']));
			if(count($tokensFromKB) < 1) {
				// Add the token
				$tokenToKB = array("corpus" => $token, "classifier_class_id" => $class['id'], "corpus_count" => 1);
				$this->db->insert("classifier_db", $tokenToKB);
			} else {
				$tokensFromKB = $tokensFromKB[0];
				$tokenScore = $tokensFromKB['corpus_count'];
				$tokenScore++;
				
				// Put the updated score on the DB
				$this->db->update("classifier_db", array("corpus_count" => $tokenScore), "id = :id", array(":id" => $tokensFromKB['id']));
			}
		}
		
		return TRUE;
	}
	
	/**
	 *	Classify the given document under the given classes based on the $userId
	 *
	 *	@param	$document	String	Text that needs to be classified
	 *	@param	$userId		Integer	UserID against whose classifiers should the text be classified
	 *
	 *	@return	String	Name of the classifier the $document belongs to.
	 *			NULL	In case of any error
	 */
	public function classify($document, $userId) {
		$classes = $this->db->select("classifier_class", "user_id = :user_id", array(":user_id" => $userId));
		$totalCountOfKB = $this->db->run("select count(*) as count from classifier_kb");
		
		$totalCountOfKB = $totalCountOfKB[0]['count'];
		// We should not have 0, that is we can't classify before knowing anything.
		if($totalCountOfKB < 1) return NULL;
		
		$priorClassScore = array();
		$classesCorpusCount = array();
		
		// Calculating the prior score for all the classes
		foreach($classes as $class) {
			$classCorpusCount = $this->db->run("select count(*) as count from classifier_kb where classifier_class_id = :class_id", array(":class_id" => $class['id']));
			$classCorpusCount = $classCorpusCount[0]['count'];
			
			$className = $class['name'];
			$classesCorpusCount[$className] = $classCorpusCount;
			$priorClassScore[$className] = $classCorpusCount / $totalCountOfKB;
		}
		
		// Clean the corpus
		$filtered_corpus = $this->filter($corpus);
		// Generate uni-grams from the cleaned corpus
		$tokens = $this->tokenise($filtered_corpus);
		
		$classScores = array();
		
		foreach($classes as $class) {
			$className = $class['name'];
			$classScores[$className] = 1;
			
			foreach($tokens as $token) {
				// Check if the token is already present in the KnowledgeBase
				$tokensFromKB = $this->db->select("classifier_db", "corpus = :token and classifier_class_id = :class_id", array(":token" => $token, ":class_id" => $class['id']));
				if(count($tokensFromKB) < 1) {
					$count = 0;
				} else {
					$tokenFromKB = $tokensFromKB[0];
					$count = $tokenFromKB['corpus_count'];
				}

				$classScores[$className] *= ($count + 1) / 
							($classesCorpusCount[$className] + $totalCountOfKB);
			}
			$classScores[$className] = $priorClassScore[$className] * $classScores[$className];
		}

		arsort($classScores);
		return key($classScores);
	}
	
	/**
	 *	Filters any stop words if found on the $document. You can also provide an array of custom words that need to be striped
	 *
	 *	@param	$document	String	Text that needs to be filtered
	 *	@param	$custom_words	Array	List of custom words that also needs to be filtered
	 *
	 *	@return	String	Filtered Twitter text
	 **/
	public function filter($document, $custom_words = array()) {
		$custom_words = '/\b(' . implode("|", $custom_words) . ')\b/';
		$initial_filter = preg_replace($this->stop_words, "", $document);
		$custom_filter = preg_replace($custom_words, "", $initial_filter);
		
		return $custom_filter;
	}
	
	/**
	 *	Tokenize the $document for getting the uni-grams
	 *
	 *	@param	$document	String	Text that needs to be tokenized
	 *
	 *	@return	Array	Tokens of the $document
	 */
	private function tokenise($document) {
			$document = strtolower($document);
			preg_match_all('/\w+/', $document, $matches);
			return $matches[0];
	}
}
