<?php
require_once(__DIR__ . "/../lib/predis/Autoloader.php");

Predis\Autoloader::register();
$predis = new Predis\Client();	// Connect to local system at 127.0.0.1 at port 6379

$predis->set('test:sample', 'testing');
echo $predis->get('test:sample');

$predis->lpush('test:list', 1);
$predis->lpush('test:list', 2);
$predis->lpush('test:list', 3);

echo $predis->rpop('test:list') . "\n";
echo $predis->rpop('test:list') . "\n";
echo $predis->rpop('test:list') . "\n";

