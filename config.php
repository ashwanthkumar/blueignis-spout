<?php

require_once(__DIR__ . "/lib/class.db.php");
require_once(__DIR__ . "/lib/Pusher.php");
require_once(__DIR__ . "/lib/predis/Autoloader.php");

/**
 *	Database related configurations
 */
$db_name = "fohubcom_blueignis";
// $db_user = "fohubcom_blueignis";
$db_user = "root";

$db_host = "localhost";
// $db_pass = "BlueIgnis@AshwanthKumar.inLabs";
$db_pass = "root";

global $db;
$db = new db("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);

global $pusher;
$pusher = new Pusher("0346ddbdb2e3f102a781", "7529bc40b114a3645d7f", "16072", true);

global $predis;
Predis\Autoloader::register();
$predis = new Predis\Client();	// Connect to local system at 127.0.0.1 at port 6379

date_default_timezone_set("Asia/Kolkata");

