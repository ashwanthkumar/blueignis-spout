<?php
/**
 *	Sentiment Filter based on excellent Naive Bayes PHP Implementation (http://phpir.com/bayesian-opinion-mining)
 *
 *	@author	Ashwanth Kumar
 *	@date	28/02/2012
 */
class SentimentFilter {
		// Entire MySQL Table goes here
        private $index = array();
		// Types of Classes, possible to get it via
		// SELECT * FROM classifier_class
        private $classes = array('p', 'n', 'neutral');
		// Count of tokens in each class of classifier
        private $classTokCounts = array('pos' => 0, 'neg' => 0);
		// How many words (after filtering the stop words) are there in the source
        private $tokCount = 0;
		// SELECT name, count(id) FROM classifier_kb WHERE class_id = (SELECT id FROM classifier_class) AND GROUP BY class_id
        private $classDocCounts = array('pos' => 0, 'neg' => 0);
		// How many total items are there in the KB?
		// SELECT * FROM classifier_kb
        private $docCount = 0;
		// Default value for all classifier_class
        private $prior = array('pos' => 0.5, 'neg' => 0.5);

        public function addToIndex($file, $class, $limit = 0) {
                $fh = fopen($file, 'r');
                $i = 0;
                if(!in_array($class, $this->classes)) {
                        echo "Invalid class specified\n";
                        return;
                }
                while($line = fgets($fh)) {
                        if($limit > 0 && $i > $limit) {
                                break;
                        }
                        $i++;
                        
                        $this->docCount++;
                        $this->classDocCounts[$class]++;
                        $tokens = $this->tokenise($line);
                        foreach($tokens as $token) {
                                if(!isset($this->index[$token][$class])) {
                                        $this->index[$token][$class] = 0;
                                }
                                $this->index[$token][$class]++;
                                $this->classTokCounts[$class]++;
                                $this->tokCount++;
                        }
                }
                fclose($fh);
        }
        
        public function classify($document) {
                $this->prior['pos'] = $this->classDocCounts['pos'] / $this->docCount;
                $this->prior['neg'] = $this->classDocCounts['neg'] / $this->docCount; 
                $tokens = $this->tokenise($document);
                $classScores = array();

                foreach($this->classes as $class) {
                        $classScores[$class] = 1;
                        foreach($tokens as $token) {
                                $count = isset($this->index[$token][$class]) ? 
                                        $this->index[$token][$class] : 0;

                                $classScores[$class] *= ($count + 1) / 
                                        ($this->classTokCounts[$class] + $this->tokCount);
                        }
                        $classScores[$class] = $this->prior[$class] * $classScores[$class];
                }
                
                arsort($classScores);
                return key($classScores);
        }

        private function tokenise($document) {
                $document = strtolower($document);
                preg_match_all('/\w+/', $document, $matches);
                return $matches[0];
        }
}
?>
