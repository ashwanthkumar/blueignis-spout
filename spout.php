<?php
require_once('./lib/Phirehose.php');
require_once('./config.php');
require_once('./Classifier.php');

/**
 *	TwitterSpout reads the stream form the Twitter Streaming API
 *
 *	@date 27/03/2012
 *	@author	Ashwanth Kumar <ashwanthkumar@googlemail.com>
 *	@version	0.1 (Alpha)
 *
 *	@service	1
 */
class TwitterSpout extends Phirehose {
	public $db;
  /**
   * Enqueue each status
   *
   * @param string $status
   */
	public function enqueueStatus($status) {
		/*
		 *	NOTE: You should NOT be processing tweets at this point in a real application, instead they should be being
		 *      enqueued and processed asyncronously from the collection process. 
		 *
		 *	I am currently queuing the JSON Status message as such on Twitter_Queue, which will then be processed by "TwitterQueueWorker" aka "Service2"
		 */
		$twitterQueue = array("tweet_dump" => $status);
		global $db;
		$db->insert("twitter_queue", $twitterQueue);
	}

    /**
     *  Method that gets called once every 60 seconds (default value) this is
     *  where we can update the track list of the consumer. We can do that as
     *  long as the thing works actually. 
     */
	protected function statusUpdate() {
		parent::statusUpdate(); // Call the base class statusUpdate anyways.It gives some stats which might be useful. 

		// Get the tracks and add some more to the list by getting them from Redis
		$tracks = $this->getTrack();

		$track_count = count($tracks);
		// Since we are limited to only 400 tracks per connection we make sure we have only 400 tracks all times
		$slots = 400 - $track_count;
		
		global $predis;
		
		$cache_tracks = $predis->llen('blueignis:new:campaign:tracks');	// Get the total number of tracks on cache
		for($i = 0; $i < $cache_tracks; $i++) {
			if($slots > 1) {
				$tracks[] = $predis->rpop('blueignis:new:campaign:tracks');
				$slots--;
			} else break;
		}
		
		echo "New Track count - " . count($tracks) . "\n";
		
		// Update the current TrackList
		$this->setTrack($tracks);
	}
}

/**
 *	This Tweeter Streaming Worker will have to be invoked as follows
 *
 *	$ php spout.php
 *
 *
 *	Spout here streams the data from the Twitter Streaming API and dumps the 
 *  JSON to "twitter_queue" which is then extracted and structured into "twitter_firehouse". 
 */
$spoutTwitter = new TwitterSpout('blueignis', 'Live4Others', Phirehose::METHOD_FILTER); // Using the @blueignis credentials
$spoutTwitter->db = $db;

// Get the first 400 list of non-archived tracks
$tracks = $db->select("tracks", "is_archived = 0 limit 0,400");
if(count($tracks) < 1) {
    echo "Spout: I am sorry but I do not find any active tracks at all. Exiting..";
    exit(1);
}

$final_tracks = array();
foreach($tracks as $track) {
    $final_tracks[] = trim($track['name']);
}

$spoutTwitter->setTrack($final_tracks);
$spoutTwitter->consume();	// Start consuming the process

