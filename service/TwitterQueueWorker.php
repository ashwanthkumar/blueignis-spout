<?php

require_once("../config.php");
require_once("../models/TwitterUser.php");
require_once("../models/TwitterFirehouse.php");
require_once("../models/TwitterFirehouse.php");

/**
 *	TwitterQueueWorker - Process the Tweet JSON responses from the MySQL DB to structure them onto the DB
 *
 *	@author	Ashwanth Kumar <ashwanth@ashwanthkumar.in>
 *	@date	29/02/2012
 *	
 *	@service 2
 */

global $pusher;
global $db;

// Keep running this as a DAEMON process
while(1) {
	$queues = $db->select("twitter_queue", "1=1 LIMIT 0,50");
	echo "Size of Queue is " . count($queues) . ". \n";
	
	if(count($queues) < 1) sleep(5);	// Sleep for 5 seconds when the queue is empty

	// Process each element of the Queue
	foreach($queues as $queue) {
		$status = $queue['tweet_dump'];
		// Decode the JSON file
		$data = json_decode($status, TRUE);
		
		// Now do all the required processing of Extracting the information out of the tweet and storing it on the DB
		if(is_array($data)) {
			if(isset($data['user'])) {
				$user = $data['user'];
				// Loading the TwitterUsers
				$twitterUser = TwitterUser::load($user, $db);
				
				// Loading into TwitterFirehouse tweets
				$tweets = TwitterFirehouse::load($data, $twitterUser, $db);
				
				$new_tweet_notification = array();
				$new_tweet_notification['user_name'] = $data['user']['screen_name'];
				$new_tweet_notification['tweet'] = $data['text'];
				$new_tweet_notification['user_img'] = $data['user']['profile_image_url'];
				$new_tweet_notification['tweet_id'] = $data['id'];
				
				$new_tweet_notification_json = json_encode($new_tweet_notification);
				// Send the entire data so that we can see what we can do about it
				$pusher_return = $pusher->trigger('blueignis-twitter-dashboard', 'new_tweet', array("message" => $new_tweet_notification_json));
				// TODO: Calculate the sentiment and stuff
			} else echo "\nSkipping the record as it does not have USER attribute with it\n";
		} else { echo "\nSkipping the record as it is not an array\n"; }
		
		// Remove the element from the queue after processing
		$db->delete("twitter_queue", "id = :id", array(":id" => $queue['id']));
	}
}

