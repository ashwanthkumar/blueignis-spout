<?php

require_once("../config.php");
require_once("../lib/uClassify.php");

$uclassify = new uClassify();

// Set these values here
$uclassify->setReadApiKey('UiysiqXw41dtu6Piai2EyVjRsE');
$uclassify->setWriteApiKey('ngUJogcUtLv9AB6wre7BFAWthc');

// Get the Pusher Instance from the configuration
global $pusher;

/**
 *	ClassifierQueueWorker - Analyses the tweets agains the uClassify Sentiment API and classifies the tweets
 *
 *	@author	Ashwanth Kumar <ashwanth@ashwanthkumar.in>
 *	@date	27/03/2012
 *	
 *	@service 4
 */

// Keep running this as a DAEMON process
while(1) {
    // We will batch process all the tweets in set of 50, since the API has a limit on the total size of the text
	$tweets = $db->run("select tweets.id as id, tweets.text as text, tweets.twitter_users_id as user_id, twitter_users.profile_image_url as profile_image_url, twitter_users.screen_name as screen_name,tweets.campaign_id as campaign_id from tweets, twitter_users where tweets.id NOT IN (select tweets_id from tweet_mood) and twitter_users.id = tweets.twitter_users_id limit 0,50");
	if(count($tweets) < 1) {
	    echo "ClassifierQueueWorker: Seems like all the tweets here are classified. I am off to a short-power nap for 150 secs. \n";
	    sleep(150);	// Sleep for 5 seconds when the queue is empty
	    continue;
	}

    $text = array();	// Contains the tweet_texts with Ids to be sent for Classification
    
    $tweet_ids = array();	// Contains the list of Tweet Ids
    $users = array();	// Contains each Tweeter information like user_id, profile_image_url, user_name
    $__tweets = array(); // Contains the tweet text
    
    $campaign = array();
	// Process each element of the Queue
	foreach($tweets as $tweet) {
	    $tweet_ids[] = $tweet['id'];
	    $text_add['id'] = $tweet['id'];
	    $text_add['text'] = $tweet['text'];
	    
	    // Gathering the required user details
	    $user = array();
	    $user['id'] = $tweet['user_id'];
	    $user['profile_image_url'] = $tweet['profile_image_url'];
	    $user['screen_name'] = $tweet['screen_name'];
	    
	    $users[] = $user;	// Add the User Ids also
	    
	    $text[] = $text_add;
	    $__tweets[] = $tweet['text'];
	    $campaign[] = $tweet['campaign_id'];
	}
	
	// uClassify does not maintain the same text id we give during 
	// classification but it returns the results in the order we
	// submitted. Atleast something works :D
	$result = $uclassify->classifyMany($text, 'Sentiment', 'uClassify');
	
	$i = 0;
    for(;$i < count($result); $i++) {
        $r = $result[$i];
        
        $tid = $tweet_ids[$i];
        
        $tweet_mood = array("tweets_id" => $tid);
        
	    $neg = $r['classification'][0]['p'];
	    $pos = $r['classification'][1]['p'];
	    if(($neg >= 0.40 && $neg <= 0.60) || ($pos >= 0.40 && $pos <= 0.60)) {
	        $tweet_mood['mood'] = "neutral";
	        $tweet_mood['score'] = ($neg + $pos) / 2; // AVG(POS+NEG)
	    } else if($neg > $pos) {
	        $tweet_mood['mood'] = "negative";
	        $tweet_mood['score'] = $neg * 100;
	    } else {
	        $tweet_mood['mood'] = "positive";
	        $tweet_mood['score'] = $pos * 100;
	    }
	    
	    // Insert the mood into the datastore
	    $db->insert("tweet_mood", $tweet_mood);
	    
	    $user = $users[$i];

		$new_tweet_notification = array();
		$new_tweet_notification['user_name'] = $user['screen_name'];
		$new_tweet_notification['tweet'] = $__tweets[$i];
		$new_tweet_notification['user_img'] = $user['profile_image_url'];
		$new_tweet_notification['tweet_id'] = $tid['id'];

		$new_tweet_notification_json = json_encode($new_tweet_notification);
		// Send the entire data so that we can see what we can do about it
		
		if($tweet_mood['mood'] == "positive") {
			$pusher_return = $pusher->trigger('blueignis-sentiment-positive-dashboard-' . $campaign[$i], 'new_tweet', array("message" => $new_tweet_notification_json));
		} else {
			$pusher_return = $pusher->trigger('blueignis-sentiment-negative-dashboard-' . $campaign[$i], 'new_tweet', array("message" => $new_tweet_notification_json));
		}

		echo "ClassifierQueueWorker: Pusher returns $pusher_return \n";
    }	
}

