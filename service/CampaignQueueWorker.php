<?php

require_once(dirname(__FILE__) . "/../config.php");
require_once(dirname(__FILE__) . "/../models/TwitterUser.php");
require_once(dirname(__FILE__) . "/../models/TwitterFirehouse.php");

/**
 *	CampaignQueueWorker - 
 *
 *	#	Started periodically by the CRON
 *	#	Searches the Redis Key List - blueignis:new:campaign
 *	#	If a value has been found there, this worker runs forever 
 *		in the background, else it exits gracefully. This way we 
 *		have a clean Process table and healthy system. 
 *	# CRON should have the following entry in it for running it every minute
 *		exec php /home/blueignis/blueignis/spout/service/CampaignQueueWorker.php > /dev/null 2>&1 &
 *
 *	@author	Ashwanth Kumar <ashwanth@ashwanthkumar.in>
 *	@date	13/04/2012
 *	
 *	@service 5
 */

// Get the global Pusher instance defined in config.php
global $pusher;
// Get the global Predis instance defined in the config.php
global $predis;

$new_campaign_key = "blueignis:new:campaign";
if($predis->llen($new_campaign_key) < 1) {
	exit;	// Exit the thread normally as nothing is required to be done
}

$pid_directory = __DIR__ . "/CampaignQueueWorkerPIDs/";
$process_pid = posix_getpid();
file_put_contents($pid_directory . $process_pid . ".pid", "");	// Write an empty pid file for the process so that we know all the process that are running. 

// Now that some campaign does exist to be processed, get its Id and vola start processing. 
$campaignToProcess = $predis->rpop($new_campaign_key);

$campaign['id'] = $campaignToProcess;	// Get the Campaign Id for which we should work for

// Get the list of tracks for the $campaign
$tracks = $db->select("tracks", "campaign_id = :cid and is_archived = 0", array(":cid" => $campaign['id']));

// Store the list of tracks for the campaign in an array so its easy to manipulate
$final_tracks = "1=2 ";
$counter = 1;
foreach($tracks as $track) {
    $final_tracks .= "or text LIKE '%" . $track['name'] . "%' ";
}

while(1) {
	$isTrackPresent = $db->run("select count(*) as cnt from twitter_firehouse where $final_tracks");
	if(count($isTrackPresent) > 0) {
		$isTrackPresent = $isTrackPresent[0];
	    if($isTrackPresent['cnt'] > 0) {
	    		$touple_count = $isTrackPresent['cnt'];
	        // Tuples are found
	        // This operation when it happens againg for a profile that contains a lot of things it will be so slow, so adding 1 more query to reduce n queries that might follow.
	        // Calculating the tweets happened so far in this campaign
	        $campaignTweetCount = $db->run("select count(*) as cnt from tweets where campaign_id = :cid", array(":cid" => $campaign['id']));
	        $campaignTweetCount = $campaignTweetCount[0]['cnt'];
	        
	        // Max limit of tweets to process at any given time
	        $number = 500;
	        if($isTrackPresent['cnt'] > 500) $number = 500;
	        else $number = $isTrackPresent['cnt']; 
	        
	        $final_tracks .= " order by posted_on limit $campaignTweetCount, $number"; // Processing only 500 tweets at a time for a campaign
	        
	        $tweets = $db->select("twitter_firehouse", $final_tracks);
	        if(count($tweets) < 1) $noTweets = true;
	        foreach($tweets as $tweet) {
	            echo "CampaignQueueWorker: Checking if Tweet#" . $tweet['id'] . " is added to the campaign. \n";
	            // Verify that the tweet does not belong to the campaign before adding
	            $isTweetPresent = $db->select("tweets", "twitter_status_id = :tstatusid and campaign_id = :cid", array(":tstatusid" => $tweet['twitter_status_id'], ":cid" => $campaign['id']));
	            
	            if(count($isTweetPresent) < 1) { // Add the tweet now, since it is not presen t
	                $tweet_data = array(
	                    "text" => $tweet['text'],
	                    "twitter_status_id" => $tweet['twitter_status_id'],
	                    "posted_on" => $tweet['posted_on'],
	                    "twitter_users_id" => $tweet['twitter_users_id'],
	                    "campaign_id" => $campaign['id']
	                );
	                
	                $db->insert("tweets", $tweet_data);
	                // echo "CampaignQueueWorker: Added the tweet to the list. \n";
	                $twitter_user = $db->select("twitter_users", "id = :userid", array(":userid" => $tweet['twitter_users_id']));
	                $twitter_user = $twitter_user[0];	// Get the first user
									        
									$new_tweet_notification = array();
									$new_tweet_notification['user_name'] = $twitter_user['screen_name'];
									$new_tweet_notification['tweet'] = $tweet['text'];
									$new_tweet_notification['user_img'] = $twitter_user['profile_image_url'];
									$new_tweet_notification['tweet_id'] = $tweet['id'];
			
									$new_tweet_notification_json = json_encode($new_tweet_notification);
									// Send the entire data so that we can see what we can do about it
									$pusher_return = $pusher->trigger('blueignis-twitter-dashboard-' . $campaign['id'], 'new_tweet', array("message" => $new_tweet_notification_json));
									echo "CampaignQueueWorker: Pusher returns $pusher_return \n";
	            } else echo "CampaignQueueWorker: Seems like it is already present. \n";
	        }
	    } else {
	        // seems to be much like no values are present inside it. Sorry nothing from the firehouse yet.
	        echo "CampaignQueueWorker: Sorry no tweets found for the campaign id " . $campaign['id'] . ". \n";
	        echo "CampaignQueueWorker: Sleep for 10 seconds before continuing. \n";
	        sleep(10);	// Sleep for 10 secs as we dont have any more mentions of tracks in local firehouse yet
	    }
	} // Else clause is not possible since we are using count(*) we will atleast get 0 inside it

	if($noTweets) {
    // seems to be much like no values are present inside it. Sorry nothing from the firehouse yet.
    echo "CampaignQueueWorker: Sorry no tweets found for the campaign id " . $campaign['id'] . ". \n";
    echo "CampaignQueueWorker: Sleep for 10 seconds before continuing. \n";
    sleep(10);	// Sleep for 10 secs as we dont have any more mentions of tracks in local firehouse yet
	}

	if($number >= 500) {
		$now = date('Y-m-d H:i:s');	
		$update_value = array("updated_at" => $now);
		// Need to update the updated_at value for the $campaign
		// $db->update("campaign", $update_value, "id = :cid", array(":cid" => $campaign['id']));
		// Stopped Updating the campaigns to avoid bottlenecks on the MySQL
	} else {
		// Seems like we have not done any substaintial work at all
		if($number == $touple_count) {
	        // seems to be much like no values are present inside it. Sorry nothing from the firehouse yet.
	        echo "CampaignQueueWorker: Sorry no tweets found for the campaign id " . $campaign['id'] . ". \n";
	        echo "CampaignQueueWorker: Sleep for 10 seconds before continuing. \n";
	        sleep(10);	// Sleep for 10 secs as we dont have any more mentions of tracks in local firehouse yet
		}
	}
}	// End of while(1)


