<?php

/**
 * ********************************************************************
 *	NOTE: This implementation is DEPRECIATED IS NOT RECOMMENDED TO USE. Use the CampaignQueueWorkerLauncher.php for more information. 
 * ********************************************************************
 */

require_once("../config.php");
require_once("../models/TwitterUser.php");
require_once("../models/TwitterFirehouse.php");
require_once("../models/TwitterFirehouse.php");

/**
 *	CampaignQueueWorker - Searches for each campaign's track against the local firehouse and updates the TwitterTweet model accordingly
 *
 *	@author	Ashwanth Kumar <ashwanth@ashwanthkumar.in>
 *	@date	27/03/2012
 *	
 *	@service 3
 */

// Get the global Pusher instance defined in Config.php
global $pusher;

// Keep running this as a DAEMON process
while(1) {
    $time = time() - 10;    // Process only campaigns which are atleast only 1 min old or its been atleast 1 min since the last update
    $now = date('Y-m-d H:i:s', $time);
    echo "CampaignQueueWorker: Max time for update - " . $now . "\n";
	$campaigns = $db->select("campaign", "updated_at <= :now", array(":now" => $now), "id");
	if(count($campaigns) < 1) {
	    echo "CampaignQueueWorker: Since there are no campaigns found. I am going to sleep, will wake up again in 5 seconds.\n";
	    sleep(5);	// Sleep for 5 seconds when the queue is empty
	    continue;
	}

	// Process each element of the Queue
	foreach($campaigns as $campaign) {
	    // Get the list of tracks for the $campaign
	    $tracks = $db->select("tracks", "campaign_id = :cid and is_archived = 0", array(":cid" => $campaign['id']));
	    
	    // Store the list of tracks for the campaign in an array so its easy to manipulate
	    $final_tracks = "1=2 ";
	    $counter = 1;
	    foreach($tracks as $track) {
	        $final_tracks .= "or text LIKE '%" . $track['name'] . "%' ";
	    }
		
		$isTrackPresent = $db->run("select count(*) as cnt from twitter_firehouse where $final_tracks");
		$isTrackPresent = $isTrackPresent[0];
		if(count($isTrackPresent) > 0) {
		    if($isTrackPresent['cnt'] > 0) {
		        // Tuples are found
		        // This operation when it happens againg for a profile that contains a lot of things it will be so slow, so adding 1 more query to reduce n queries that might follow.
		        // Calculating the tweets happened so far in this campaign
		        $campaignTweetCount = $db->run("select count(*) as cnt from tweets where campaign_id = :cid", array(":cid" => $campaign['id']));
		        $campaignTweetCount = $campaignTweetCount[0]['cnt'];
		        
		        $final_tracks .= " order by posted_on limit $campaignTweetCount, 500"; // Processing only 500 tweets at a time for a campaign
		        
		        $tweets = $db->select("twitter_firehouse", $final_tracks);
		        foreach($tweets as $tweet) {
		            echo "CampaignQueueWorker: Checking if Tweet#" . $tweet['id'] . " is added to the campaign. \n";
		            // Verify that the tweet does not belong to the campaign before adding
		            $isTweetPresent = $db->select("tweets", "twitter_status_id = :tstatusid and campaign_id = :cid", array(":tstatusid" => $tweet['twitter_status_id'], ":cid" => $campaign['id']));
		            
		            if(count($isTweetPresent) < 1) { // Add the tweet now, since it is not presen t
		                $tweet_data = array(
		                    "text" => $tweet['text'],
		                    "twitter_status_id" => $tweet['twitter_status_id'],
		                    "posted_on" => $tweet['posted_on'],
		                    "twitter_users_id" => $tweet['twitter_users_id'],
		                    "campaign_id" => $campaign['id'],
		                    "source" => $tweet['source']
		                );
		                
		                $db->insert("tweets", $tweet_data);
		                echo "CampaignQueueWorker: Added the tweet to the list. \n";
		                $twitter_user = $db->select("twitter_users", "id = :userid", array(":userid" => $tweet['twitter_users_id']));
		                $twitter_user = $twitter_user[0];	// Get the first user
		                
						$new_tweet_notification = array();
						$new_tweet_notification['user_name'] = $twitter_user['screen_name'];
						$new_tweet_notification['tweet'] = $tweet['text'];
						$new_tweet_notification['user_img'] = $twitter_user['profile_image_url'];
						$new_tweet_notification['tweet_id'] = $tweet['id'];
				
						$new_tweet_notification_json = json_encode($new_tweet_notification);
						// Send the entire data so that we can see what we can do about it
						$pusher_return = $pusher->trigger('blueignis-twitter-dashboard-' . $campaign['id'], 'new_tweet', array("message" => $new_tweet_notification_json));
						echo "CampaignQueueWorker: Pusher returns $pusher_return \n";
		            } else echo "CampaignQueueWorker: Seems like it is already present. \n";
		        }
		    } else {
		        // seems to be much like no values are present inside it. Sorry nothing from the firehouse yet.
		        echo "CampaignQueueWorker: Sorry no tweets found for the campaign id " . $campaign['id'] . " tracks\n";
		    }
		} // Else clause is not possible since we are using count(*) we will atleast get 0 inside it
	}

    $now = date('Y-m-d H:i:s');	
    $update_value = array("updated_at" => $now);
	// Need to update the updated_at value for the $campaign
	$db->update("campaign", $update_value, "id = :cid", array(":cid" => $campaign['id']));
}

