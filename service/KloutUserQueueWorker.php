<?php

/**
 *	KloutUserQueueWorker - 
 *			Gets the User's Klout Score, Klout Class, Klout Description for Twitter Users.
 *
 *	Notes - This workers should be added to Cron to be executed like in every day only once. It will automatically exit upon performing 10K twitter user updates. 
 *
 *	@author	Ashwanth Kumar <ashwanthkumar@googlemail.com>
 *	@version	0.1	
 *	@date	13/04/2012
 */

require_once(__DIR__ . "/../config.php");

// Get the PDOWrapper instance for working with the DB
global $db;

// Having an array of API Keys
$apikeys = array(
	'7amfe2e5ev373ru4y9yp5a5w',	// Social Heat Key
	'jmdukpgrrjs8bdm2fywhv6x7',	// BlueIgnis Key 
);

/**
 *	Method used for accessing the Klout API show:users to get the details about the users
 *
 *	@param	$apiKey	API Key that needs to be registered at http://developer.klout.com/apps/register
 */
function accessKloutAPI($apiKey = '7amfe2e5ev373ru4y9yp5a5w', $users = array()) {
	$urlToProcess = "http://api.klout.com/1/users/show.json?users=" . implode($users, ",") . "&key=" . $apiKey;
	
	$kloutResponse = file_get_contents($urlToProcess);
	
	return json_decode($kloutResponse, TRUE);
}

$users = $db->select("twitter_users", "onklout = 0 or kscore = NULL order by id limit 10");

$names = array();
foreach($users as $user) $names[] = $user['screen_name'];

$klout_scores = accessKloutAPI($apikeys[1], $names);

foreach($klout_scores['users']['score'] as $scores) {
	$update_user = array(
		"onklout" => true,
		"kscore" => $scores['kscore'],
		"kclass" => $scores['kclass'],
		"kdesc" => $scores['kscore_description'],
	);
	
	$db->update("twitter_users", $update_user, "screen_name = :name", array(":name" => $scores['twitter_screen_name']));
	echo "KloutUserQueueWorker: Updating Twitter user " . $scores['twitter_screen_name'] . " \n";
}

